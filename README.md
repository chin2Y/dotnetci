# Basic CI Pipeline for .NET Core Application

![Gitlab-dotnetCore](/ci-images/gitlab-dotnetcore.png)

GitLab CI utilizes any docker container to build and deploy any app.

* Create Basic Application: Sample project structure
```
master
    |- src -
           |- MVC
           |- MVCUnitTest
    |- .gitlab-ci.yml
    |- .gitignore
```
* Create Pipeline : *.gitlab-ci.yml* file at the root of the project. 

Going through the yml file.
```
image: microsoft/dotnet:latest
```
Using dotnet latest image provided by microsoft which will build our application. 
We will have a container running this image and will be running our build and package scripts in that container.
We can add variables, before scripts if required.
for. eg. here we have created an variable *test* to indicate the unit test folder and in *before_script* we just navigated inside the src folder as all our project files are inside that folder. 

```
variables:
    test: "MVCUnitTest"

before_script:
    - "cd src"
```

Then we will define stages like ""build, test, and release"". 

```
stages:
   - build
   - test
   - release
```
Then we will add scripts to be executed in each stage
Stage heading can be anything, however the property **stage** must have value from **stages** defined above

* Build stage
Here we have specified stage heading and stage property both as *build*
In the script we will navigate to the directory where our application resides, restore packages and build the project using *dotnet build* command 
```
build:
    stage: build
    script:
        - "cd MVC"
        - "dotnet restore"
        - "dotnet build"

```
* Test Stage
If build stage Passes, the pipeline will execute next job *test*. This will execute the unit tests.
In the script we are navigating to the unittest directory and running dotnet test command which execute the tests.
```
test:
    stage: test
    script: 
        - "cd $test"
        - "dotnet test"

```
* Release Stage
In release stage we will get the package for deployment in the artifacts.
Here we have added *only: master* to publish only from master branch.
Also specified the artifacts path where the published output will be stored. 
*dotnet publish* command along with capturing the generated artifacts.
```
release:
    stage: release
    only:
        - master
    artifacts:
        paths:
            - src/publish/
    script:
        - dotnet publish -c Release -o ../publish MVC/TestMVC.csproj
```

Complete .gitlab-ci.yml config will look like:

```
image: microsoft/dotnet:latest

stages:
    - build
    - test
    - release

variables:
    test: "MVCUnitTest"

before_script:
    - "cd src"
    
build:
    stage: build
    script:
        - "cd MVC"
        - "dotnet restore"
        - "dotnet build"

test:
    stage: test
    script: 
        - "cd $test"
        - "dotnet test"

release:
    stage: release
    only:
        - master
    artifacts:
        paths:
            - src/publish/
    script:
        - dotnet publish -c Release -o ../publish MVC/TestMVC.csproj
```
You can navigate to CI/CD tab on GitLab to view the pipeline
This pipeline will be trigerred for every commit made to gitlab.

![ci-1](/ci-images/ci-1.PNG)
--------------------------------------------------
![ci-2](/ci-images/ci-2.PNG)
--------------------------------------------------
![ci-3](/ci-images/ci-3.PNG)

> 
